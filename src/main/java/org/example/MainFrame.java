package org.example;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class MainFrame extends JFrame {
    private CardLayout cardLayout;
    private JPanel cardPanel;

    private Connection connection;

    private DefaultTableModel tableModel;

    class User {
        private int id;

        private String username;
        private String nim;
        private String prodi;

        public User(String username, String nim, String prodi) {
            this.username = username;
            this.nim = nim;
            this.prodi = prodi;
        }

        public User(int id, String username, String nim, String prodi) {
            this.id = id;
            this.username = username;
            this.nim = nim;
            this.prodi = prodi;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getNim() {
            return nim;
        }

        public void setNim(String nim) {
            this.nim = nim;
        }

        public String getProdi() {
            return prodi;
        }

        public void setProdi(String prodi) {
            this.prodi = prodi;
        }
    }

    public MainFrame(Connection connection) {
        this.connection = connection;

        setTitle("GUI Final Project");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(400, 300);
        setLocationRelativeTo(null);

        cardLayout = new CardLayout();
        cardPanel = new JPanel(cardLayout);

        // Page A - Main Page
        JPanel pageA = new JPanel();
        JButton saveItemButton = new JButton("Save Item");
        JButton getItemButton = new JButton("Get Item");

        saveItemButton.addActionListener((e) -> {
            cardLayout.show(cardPanel, "pageB");
        });

        getItemButton.addActionListener((e) -> {
            try{
                boolean hasData = true;
                if (hasData) {
                    showUserTable(getList());
                    cardLayout.show(cardPanel, "pageC");
                } else {
                    JOptionPane.showMessageDialog(MainFrame.this, "Tidak ada data", "Error", JOptionPane.ERROR_MESSAGE);
                }
            } catch (Exception ex){
                ex.printStackTrace();
                System.out.println("Error occurred with message: " + ex.getMessage());
            }
        });

        pageA.add(saveItemButton);
        pageA.add(getItemButton);

        // Page B - Save Item Page
        JPanel pageB = new JPanel(new GridLayout(5, 2));
        JTextField namaField = new JTextField(20);
        JTextField nimField = new JTextField(20);
        JTextField prodiField = new JTextField(20);
        JButton submitButton = new JButton("Submit");
        JButton backButtonB = new JButton("Back");

        submitButton.addActionListener((e) -> {
            // Get data from fields
            String name = namaField.getText();
            String nim = nimField.getText();
            String prodi = prodiField.getText();

            User user = new User(name, nim, prodi);
            try{
                insertMahasiswa(user);
            } catch (Exception ex){
                ex.printStackTrace();
                System.out.println("Error occurred with message: " + ex.getMessage());
            }

            // Go back to Page A
            cardLayout.show(cardPanel, "pageA");
        });

        backButtonB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cardLayout.show(cardPanel, "pageA");
            }
        });

        pageB.add(new JLabel("Nama Mahasiswa: "));
        pageB.add(namaField);
        pageB.add(new JLabel("NIM: "));
        pageB.add(nimField);
        pageB.add(new JLabel("Prodi: "));
        pageB.add(prodiField);
        pageB.add(new JLabel()); // Empty label for alignment
        pageB.add(submitButton);
        pageB.add(new JLabel()); // Empty label for alignment
        pageB.add(backButtonB);

        // Page C - Get Item Page
        JPanel pageC = new JPanel();
        JLabel noDataLabel = new JLabel("Tidak ada data");
        JButton backButtonC = new JButton("Back");

        backButtonC.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cardLayout.show(cardPanel, "pageA");
            }
        });

        pageC.add(noDataLabel);
        pageC.add(backButtonC);
        pageC.setLayout(new BorderLayout());

        // Table to display user data
        JTable table = new JTable();
        JScrollPane scrollPane = new JScrollPane(table);
        pageC.setLayout(new BorderLayout());
        pageC.add(scrollPane, BorderLayout.CENTER);
        pageC.add(backButtonC, BorderLayout.SOUTH);

        // Initialize table model
        tableModel = new DefaultTableModel();
        tableModel.addColumn("ID");
        tableModel.addColumn("Username");
        tableModel.addColumn("NIM");
        tableModel.addColumn("Prodi");
        table.setModel(tableModel);

        cardPanel.add(pageA, "pageA");
        cardPanel.add(pageB, "pageB");
        cardPanel.add(pageC, "pageC");

        add(cardPanel);

        cardPanel.add(pageA, "pageA");
        cardPanel.add(pageB, "pageB");
        cardPanel.add(pageC, "pageC");

        add(cardPanel);
    }

    private void showUserTable(List<User> data) {

        // Clear existing rows
        tableModel.setRowCount(0);

        // Populate table with user data
        for (User user : data) {
            Object[] rowData = {user.getId(), user.getUsername(), user.getNim(), user.getProdi()};
            tableModel.addRow(rowData);
        }
    }



    private boolean insertMahasiswa(User user) throws Exception{
        PreparedStatement preparedStatement = connection.prepareStatement(
                "INSERT INTO `mahasiswa`(username, nim, prodi) VALUES (?, ?, ?)"
        );
        preparedStatement.setString(1, user.username);
        preparedStatement.setString(2, user.nim);
        preparedStatement.setString(3, user.prodi);
        return preparedStatement.execute();
    }

    private List<User> getList() throws SQLException {
        PreparedStatement statement = connection.prepareStatement(
                "SELECT * FROM `mahasiswa`"
        );
        ResultSet res = statement.executeQuery();
        List<User> listUser = new ArrayList<>();
        while (res.next()){
            listUser.add(new User(res.getInt("id"), res.getString("username"), res.getString("nim"), res.getString("prodi")));
        }
        return listUser;
    }

    public static void main(String[] args) {
        String username = "root"; // adjust it by your mysql configuration
        String password = "root"; // adjust it by your mysql configuration
        String jdbcUrl = String.format("jdbc:mysql://%s:%s@localhost:3306/gui", username, password);
        try{
            Connection conn = DriverManager.getConnection(jdbcUrl);

            SwingUtilities.invokeLater(() -> {
                MainFrame frame = new MainFrame(conn);
                frame.setVisible(true);
            });
        } catch (Exception ex){
            ex.printStackTrace();
            System.out.println("Error occurred with message: " + ex.getMessage());
        }
    }
}


